import { Pipe, PipeTransform } from '@angular/core';

// gets a number in seconds and transforms it to hh mm ss
@Pipe({ name: 'secondsToTime' })
export class SecondsToTime implements PipeTransform {
    transform(delta: number): string {

        /*
        let d = new Date(0,0,0,0,0,0,0);
        console.log(d);
        d.setSeconds(timeLeftSeconds *1000);
        console.log(d);
        console.log(timeLeftSeconds);
        console.log(d.getDate());
         */

        let date: string = '';
        let onlySeconds: boolean = true;

        //let months: number = d.getMonth();
        let months: number = Math.floor(delta / 2629746);
        delta -= months * 2629746;
        if (months) {
            onlySeconds = false;
            date += months + ' month' + (months > 1 ? 's' : '') + ', ';
        }
        //let days: number = d.getDate();
        let days: number = Math.floor(delta / 86400);
        let weeks: number = 0;
        delta -= days * 86400;
        if (days) {
            onlySeconds = false;
            if (days > 7) {
                weeks = Math.trunc(days / 7);
                date += weeks + ' week' + (weeks > 1 ? 's' : '') + ', ';
                days = days % 7;
                if (days) { date += days + ' day' + (days > 1 ? 's' : '') + ' and ' }
            } else {
                if (days) { date += days + ' day' + (days > 1 ? 's' : '') + ' and '; }
            }
        }
        //let hours: number = d.getHours();
        let hours: number = Math.floor(delta / 3600);
        delta -= hours * 3600;
        if (hours) {
            onlySeconds = false;
            date += (hours <= 9 ? '0' : '') + hours + ':';
        }
        else {
            date += '00:';
        }
        //let minutes: number = d.getMinutes();
        let minutes: number = Math.floor(delta / 60);
        delta -= minutes * 60;
        if (minutes) {
            onlySeconds = false;
            date += (minutes <= 9 ? '0' : '') + minutes + ':';
        }
        else {
            date += '00:';
        }
        //if (!onlySeconds) { date += ' and '; }
        //let seconds: number = d.getSeconds();
        let seconds: number = delta;
        date += (seconds <= 9 ? '0' : '') + seconds;

        return date;
    }
}
