import { Pipe, PipeTransform } from '@angular/core';
import { ChangeMessageComponent } from '../settings/message.component';

@Pipe({
    name: 'characterCountdown',
    pure: true
})

export class CharacterCountdown implements PipeTransform {
  transform(text: string, args: number) {
    let maxLength = 140;
    let length = text.length;
    
    return (maxLength - length);
  }
}