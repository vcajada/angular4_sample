import { Pipe, PipeTransform  } from '@angular/core';

// limits text to x chars and replaces ending with '...'
@Pipe({name: 'limitText'})
export class LimitText implements PipeTransform {
    transform(text: string): string {
        if (text.length > 20) {
            let slicedText = text.slice(0, 18);
            slicedText += '...';
            return slicedText;
        }
        return text;
    }
}
