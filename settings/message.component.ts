import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ActivatedRoute, ParamMap  } from '@angular/router';
import { DataService } from '../services/data.service';
import { UserService } from '../services/user.service';

@Component({
    templateUrl: './message.html',
})

export class ChangeMessageComponent implements OnInit {

    isRequesting: boolean = false;
    errors: string;
    message: any = {};
    messageIndex: any = '';
    currentUser = JSON.parse(sessionStorage.getItem('current_user'));
    
    maxlength = 20000;

    constructor(private router: Router, private dataService: DataService, private activatedRoute: ActivatedRoute, private userService: UserService) {}

    ngOnInit(): void {
        let myThis = this;
        this.activatedRoute.params.subscribe(params => {
            myThis.messageIndex = params['id'];
        });

        if (this.messageIndex != 'new') {
            this.message = myThis.currentUser.Messages[this.messageIndex];
        }
    }

    updateMessage() {

        if (!this.message.SendTo || !this.message.Text) {
            this.errors = 'Please fill all fields';
            return;
        }

        this.isRequesting = true;
        this.userService.isRequesting = true;

        let requestBodyMessage = {};

        if (this.messageIndex == 'new') {
            requestBodyMessage = {
                UserId: this.currentUser.Id,
                Message: this.message.Text,
                SendTo: this.message.SendTo,
                Delay: this.message.Delay,
            };

            this.dataService.authPostData('api/users/createmessage', requestBodyMessage, (response) => {
                this.dataService.authGetData(`api/users/user?id=${this.currentUser.Id}`, (currentUser) => {
                    sessionStorage.setItem('current_user', JSON.stringify(currentUser));

                    this.isRequesting = false;
                    this.userService.isRequesting = false;
                    this.router.navigate(['/change-messages']);
                }, (error) => {
                    this.isRequesting = false;
                    this.userService.isRequesting = false;
                    this.errors = error;
                });
            }, (error) => {
                this.isRequesting = false;
                this.userService.isRequesting = false;
                this.errors = error;
            });
        } else {
            requestBodyMessage = {
                UserId: this.currentUser.Id,
                MessageId: this.message.Id,
                Message: this.message.Text,
                SendTo: this.message.SendTo,
                Delay: this.message.Delay,
            };

            this.dataService.authPostData('api/users/updatemessage', requestBodyMessage, (response) => {
                this.errors = '';
                this.currentUser.Messages[this.messageIndex].Text = this.message.Text;
                this.currentUser.Messages[this.messageIndex].Delay = this.message.Delay;
                this.currentUser.Messages[this.messageIndex].SendTo = this.message.SendTo;
                sessionStorage.setItem('current_user', JSON.stringify(this.currentUser));
                this.isRequesting = false;
                this.userService.isRequesting = false;
                this.router.navigate(['/change-messages']);
            }, (error) => {
                this.isRequesting = false;
                this.userService.isRequesting = false;
                this.errors = error;
            });
        }
    }
}
