import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DataService } from '../services/data.service';
import { UserService } from '../services/user.service';

@Component({
    templateUrl: './actions.html',
})

export class ChangeActionsComponent implements OnInit {

    isRequesting: boolean = false;
    errors: string;
    days: number = 0;
    subscriptionType: number = this.userService.getSubscription();

    isFree: boolean = false;
    upgrade: boolean = false;
    upgradeTolerance: boolean = false;

    constructor(private router: Router, private dataService: DataService, private userService: UserService) { }

    currentUser = JSON.parse(sessionStorage.getItem('current_user'));

    messages: { id: number, delay: string, sendTo: string, message: string, action: string }[] = [];

    ngOnInit() {

        this.days = Math.trunc(this.currentUser.AbsenceTolerance / 86400);

        if (this.currentUser.Messages.length) {
            let scope = this;
            this.currentUser.Messages.forEach(function (message, index) {
                scope.messages.push({ id: message.Id, delay: message.Delay, sendTo: message.SendTo, message: message.Text, action: 'update' });
            });
        }
    }

    updateActions() {
        this.isRequesting = true;
        this.userService.isRequesting = true;

        let absenceToleranceInSeconds = this.days * 86400;
        this.currentUser.AbsenceTolerance = absenceToleranceInSeconds;

        let requestBody = {
            Id: this.currentUser.Id,
            AbsenceTolerance: absenceToleranceInSeconds,
            GiveCall: this.currentUser.GiveCall,
            SendEMailTolerance: this.currentUser.SendEmailTolerance
        }

        this.dataService.authPostData('api/users/updateactions', requestBody, (response) => {
            this.errors = '';
            sessionStorage.setItem('current_user', JSON.stringify(this.currentUser));
            this.isRequesting = false;
            this.userService.isRequesting = false;
            this.router.navigate(['/settings']);
        }, (error) => {
            this.isRequesting = false;
            this.userService.isRequesting = false;
            this.errors = error;
        });

    }

    addMessage() {
        this.router.navigate(['/change-message', 'new']);
    }

    deleteMessage(index) {

        this.isRequesting = true;
        this.userService.isRequesting = true;

        let message = this.currentUser.Messages[index];
        console.log(message);
        let requestBodyMessage = {
            UserId: this.currentUser.Id,
            MessageId: message.Id
        };

        this.dataService.authPostData('api/users/deletemessage', requestBodyMessage, (response) => {
            this.errors = '';
            this.messages.splice(index, 1);
            this.currentUser.Messages.splice(index, 1);
            this.isRequesting = false;
            this.userService.isRequesting = false;
        }, (error) => {
            this.isRequesting = false;
            this.userService.isRequesting = false;
            this.errors = error;
        });
    }
}
