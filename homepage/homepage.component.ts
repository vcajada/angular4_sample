import { Component, OnInit } from '@angular/core';

import { DataService } from '../services/data.service';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { SecondsToTime } from '../pipes/seconds-time.pipe';

@Component({
    templateUrl: './homepage.html',
})

export class HomepageComponent implements OnInit {


    checkinTimer: number = 0;
    checkinTolerance?: number = 0;
    checkinTitle: string = 'until next PoL';
    message: string = '';
    currentUser = JSON.parse(sessionStorage.getItem('current_user'));
    interval: any;
    polButtonText: string = 'alive';

    constructor(private userService: UserService, private dataService: DataService, private router: Router) { }

    isRequesting: boolean = false;
    errors: string;

    ngOnInit() {
        this.isRequesting = true;
        this.userService.isRequesting = true;
        this.dataService.authGetData(`api/users/${sessionStorage.getItem('user_id')}`, (currentUser) => {
            sessionStorage.setItem('current_user', JSON.stringify(currentUser));
            this.currentUser = JSON.parse(sessionStorage.getItem('current_user'));
            this.calcCheckinTimer();

            let self = this;
        }, (error) => {
            this.isRequesting = false;
            this.userService.isRequesting = false;
            if (!this.userService.isLoggedIn()) {
                this.router.navigate(['/login']);
            }
        });
    }

    checkIn() {
        this.isRequesting = true;
        this.userService.isRequesting = true;

        let requestBody = { id: sessionStorage.getItem('user_id') };
        this.dataService.authPostData('api/users/timerupdate', requestBody, (currentCheckin) => {
            this.currentUser.Frequency = currentCheckin.Frequency;
            this.currentUser.LastAccess = currentCheckin.LastAccess;
            sessionStorage.setItem('current_user', JSON.stringify(this.currentUser));
            this.calcCheckinTimer();
        }, (error) => {
            this.isRequesting = false;
            this.userService.isRequesting = false;
            this.errors = error;
        });
    }

    calcCheckinTimer() {

        let nowInSecs = Math.round(new Date().getTime() / 1000);

        let lastAccess = new Date(this.currentUser.LastAccess);
        var secsOfLastAccessDay = lastAccess.getSeconds() + (60 * lastAccess.getMinutes()) + (60 * 60 * lastAccess.getHours());

        let nextCheckin = Math.round(Date.parse(this.currentUser.LastAccess) / 1000) - secsOfLastAccessDay + this.currentUser.Frequency + this.currentUser.TimeOfDay;
        let nextTolerance = Math.round(Date.parse(this.currentUser.LastAccess) / 1000) - secsOfLastAccessDay + this.currentUser.Frequency + this.currentUser.TimeOfDay + this.currentUser.AbsenceTolerance;
        this.checkinTimer = nextCheckin - nowInSecs;
        this.checkinTolerance = nextTolerance - nowInSecs;

        if (!this.interval) {
            let myThis = this;
            this.interval = setInterval(function () {
                if (myThis.checkinTimer < 0) {
                    myThis.checkinTolerance--;
                }
                else {
                    myThis.checkinTimer--;
                }
            }, 1000)
        }
        this.isRequesting = false;
        this.userService.isRequesting = false;
    }
}
