import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { DataService } from './data.service'
import { Router } from '@angular/router';

@Injectable()
export class UserService {

    public isRequesting: boolean = false;

    constructor(private http: Http, private dataService: DataService, private router: Router) { }

    login(username: string, password: string, utcOffset: number, fn: ((value) => void), fnError: ((error) => void) = null): void {
        let serviceRequestBody: { username: string, password: string, utcOffset: number } = { username: username, password: password, utcOffset: utcOffset };
        this.dataService.postData('api/accounts/login', serviceRequestBody, fn, fnError);
    }

    register(registerModel: any, fn: ((value) => void), fnError: ((error) => void) = null): void {
        this.dataService.postData('api/accounts/register', registerModel, fn, fnError);
		/*
        registerModel.auth_token = registerModel.username + 'token';
        registerModel.Frequency = 10000;
        let nowInSecs = Math.round(new Date().getTime() /1000);
        registerModel.LastAccess = nowInSecs;
        registerModel.phonenumber = 123123123;
        registerModel.message = 'I dieded standard';

        if (registerModel.password == registerModel.confirmPassword) {
            delete registerModel.confirmPassword;
            this.dataService.postData(`api/users`, registerModel, fn, fnError);
        }
		*/
    }

    logout(): void {
        sessionStorage.removeItem('authentication_token');
        sessionStorage.removeItem('user_id');
        sessionStorage.removeItem('subsc');
        sessionStorage.removeItem('current_user');
        this.router.navigate(['/login']);
    }

    isLoggedIn(): boolean {
        return !!sessionStorage.getItem('authentication_token')
            && !!sessionStorage.getItem('user_id')
            && !!sessionStorage.getItem('current_user');
    }

    updateActive(id: number, active: boolean, fn: ((value) => void), fnError: ((error) => void) = null): void {
        let serviceRequestBody: { Id: number, Active: boolean } = { Id: id, Active: active };
        this.dataService.authPostData('api/users/active', serviceRequestBody, fn, fnError);
    }

    XpressPoL(id: number, XpressPoL: boolean, fn: ((value) => void), fnError: ((error) => void) = null): void {
        let serviceRequestBody: { Id: number, XpressPoL: boolean } = { Id: id, XpressPoL: XpressPoL };
        this.dataService.authPostData('api/users/xpresspol', serviceRequestBody, fn, fnError);
    }

    getSubscription(): number {
        return +sessionStorage.getItem('subsc');
    }
}
