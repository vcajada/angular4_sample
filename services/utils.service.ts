// TODO include somewhere else not so vague
import { Injectable }       from '@angular/core';

@Injectable()
export class UtilsService {
     
    handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // TODO handle errors properly
        return Promise.reject(error.message || error);
    }
}
