import 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DataService {
    constructor(private http: Http) { }

    //private serverUrl = 'https://app.globalproofoflife.com/';
    //private serverUrl = 'http://poofoflife.eu-west-2.elasticbeanstalk.com/';
    private serverUrl = 'http://localhost:5000/';

    private getPostDataObservable(url: string, data: any, auth: boolean): Observable<any> {
        const body = JSON.stringify(data);
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        if (auth) {
            let authToken = sessionStorage.getItem('authentication_token');
            headers.append('Authorization', `Bearer ${authToken}`);
        }

        var ret = this.http.post(url, body, { headers: headers }).map(response => {
            if (this.isJson(response)) {
                return response.json();
            }
            else {
                return response.text();
                // true;
            }
        }).catch(this.handleError);
        return ret;
    }

    private getPutDataObservable(url: string, data: any, auth: boolean): Observable<any> {
        const body = JSON.stringify(data);
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        if (auth) {
            let authToken = sessionStorage.getItem('authentication_token');
            headers.append('Authorization', `Bearer ${authToken}`);
        }

        var ret = this.http.put(url, body, { headers: headers }).map(response => response.json()).catch(this.handleError);
        return ret;
    }

    private getGetDataObservable(url: string, auth: boolean): Observable<any> {
        const headers = new Headers();
        if (auth) {
            let authToken = sessionStorage.getItem('authentication_token');
            headers.append('Authorization', `Bearer ${authToken}`);
        }
        var ret = this.http.get(url, { headers: headers }).map(response => {
            if (this.isJson(response)) {
                return response.json();
            }
            else {
                return response.text();
                // true;
            }
        }).catch(this.handleError);
        return ret;
    }

    private getDeleteDataObservable(url: string, auth: boolean): Observable<any> {
        const headers = new Headers();
        var ret = this.http.delete(url);
        if (auth) {
            let authToken = sessionStorage.getItem('authentication_token');
            headers.append('Authorization', `Bearer ${authToken}`);
        }
        return ret;
    }

    private reportError(error): void {
        alert('something went wrong: ' + JSON.stringify(error));
    }

    private subscribe(observable: Observable<any>, fn: ((value) => void), fnError: ((error) => void) = null): void {
        if (fn == null)
            fn = (p => { });
        if (fnError == null)
            fnError = this.reportError;
        observable.subscribe(fn, fnError);
    }


    public postData(url: string, data: any, fn: ((value) => void), fnError: ((error) => void) = null): void {
        var observable = this.getPostDataObservable(this.serverUrl + url, data, false);
        this.subscribe(observable, fn, fnError);
    }

    public putData(url: string, data: any, fn: ((value) => void)): void {
        var observable = this.getPutDataObservable(url, data, false);
        this.subscribe(observable, fn);
    }

    public getData(url: string, fn: ((value) => void), fnError: ((error) => void) = null): void {
        var observable = this.getGetDataObservable(this.serverUrl + url, false);
        this.subscribe(observable, fn, fnError);
    }

    public deleteData(url: string, fn: ((value) => void) = null): void {
        var observable = this.getDeleteDataObservable(url, false);
        this.subscribe(observable, fn);
    }

    public authPostData(url: string, data: any, fn: ((value) => void), fnError: ((error) => void) = null): void {
        var observable = this.getPostDataObservable(this.serverUrl + url, data, true);
        this.subscribe(observable, fn, fnError);
    }

    public authPutData(url: string, data: any, fn: ((value) => void)): void {
        var observable = this.getPutDataObservable(url, data, true);
        this.subscribe(observable, fn);
    }

    public authGetData(url: string, fn: ((value) => void), fnError: ((error) => void) = null): void {
        var observable = this.getGetDataObservable(this.serverUrl + url, true);
        this.subscribe(observable, fn, fnError);
    }

    public authDeleteData(url: string, fn: ((value) => void) = null): void {
        var observable = this.getDeleteDataObservable(url, true);
        this.subscribe(observable, fn);
    }

    // UTILS

    private handleError(error: any) {
        if (error.status == 401) {
            sessionStorage.removeItem('authentication_token');
            sessionStorage.removeItem('user_id');
            sessionStorage.removeItem('subsc');
            sessionStorage.removeItem('intOffset');
            sessionStorage.removeItem('current_user');
            return Observable.throw("Sorry, you don't have authorization to access this resource. Login first");
        }
        var applicationError = error.headers.get('Application-Error');

        // either applicationError in header or model error in body
        if (applicationError) {
            return Observable.throw(applicationError);
        }

        var modelStateErrors: string = '';
        var serverError = error.json();

        if (!serverError.type) {
            for (var key in serverError) {
                if (serverError[key])
                    modelStateErrors += serverError[key] + '\n';
            }
        }

        modelStateErrors = modelStateErrors = '' ? null : modelStateErrors;
        return Observable.throw(modelStateErrors || 'Server error');
    }

    private isJson(value: any) {
        return /\bapplication\/json\b/.test(value.headers.get('Content-Type'));
    }
}
