import { Injectable }       from '@angular/core';
import { Http, Headers }     from '@angular/http';

import { Checkin }          from '../models/checkin';

import 'rxjs/add/operator/toPromise';

import { UtilsService }     from './utils.service'

@Injectable()
export class CheckinService {

    private checkinUrl: string = 'api/checkin';
    private jsonHeaders = new Headers({'Content-Type': 'application/json'});
    currentCheckin: null | Checkin;

    constructor(private http: Http, private utilsService: UtilsService) {}

    loadCheckin(user_id: number): Promise<Checkin> {
        return this.getCheckin(user_id).then(currentCheckin => {
            this.currentCheckin = currentCheckin;
            return this.currentCheckin;
        });
    }
    getCheckin(user_id: number): Promise<Checkin> {
        const url = this.checkinUrl + '?user_id=' + user_id;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json().data as Checkin)
            .catch(this.utilsService.handleError);
    }
    updateCheckin(): Promise<Checkin> {
        const url = this.checkinUrl + '/' + this.currentCheckin.id;

        return this.http
            .put(url, JSON.stringify(this.currentCheckin), {headers: this.jsonHeaders})
            .toPromise()
            .then(() => this.currentCheckin)
            .catch(this.utilsService.handleError);
    }
}
